//
//  ViewController.swift
//  Is It Prime?
//
//  Created by Victor Hernandez on 11/5/14.
//  Copyright (c) 2014 Compubility. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var userInput: UITextField!
    
    @IBOutlet weak var resultLabel: UILabel!
    @IBAction func tellMePressed(sender: AnyObject) {
        var numberInput = userInput.text.toInt()
        // a valid number is entered
        if (numberInput > 0){
            // change display text based on the status returned by the function
            if (isitPrime(numberInput!)){
                resultLabel.text = "\(numberInput!) is Prime!";
            }
            else {
                resultLabel.text = "\(numberInput!) is not Prime!";
            }
        }
            // handle invalid integer
        else if (numberInput <= 0 && numberInput != nil){
            resultLabel.text = "Please enter a number greater than 0"
        }
            // handle no input
        else{
            resultLabel.text = "Please enter a value in the field."
        }

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        resultLabel.text = "";
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

