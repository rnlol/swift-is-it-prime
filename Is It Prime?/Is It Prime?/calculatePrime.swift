//
//  calculatePrime.swift
//  Is It Prime?
//
//  Created by Victor Hernandez on 11/12/14.
//  Copyright (c) 2014 Compubility. All rights reserved.
//

import Foundation


// This file is the functionality of "Is It Prime"

// Function for determnining whether input is prime

func isitPrime(var number:Int) -> Bool{
    // 1 is not prime
    if (number == 1 ){
        return false;
    }
    for (var divisor = 2 ; divisor < number ; divisor++){
        // if number is evenly divisible by the number at any point, the number is not prime
        if (number % divisor == 0){
            return false;
        }
    }
    // if false wasn't returned at this point, the number is prime
    return true;
}